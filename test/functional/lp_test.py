import os
import shutil
import tempfile
from subprocess import Popen
import filecmp
import socket

#copy test data to /tmp directory
current_dir = os.path.dirname(os.path.realpath(__file__))
path_to_data_file = os.path.join(current_dir,"data.txt")
temp_dir = tempfile.gettempdir()
path_to_temp_original = os.path.join(temp_dir,"data.txt")
shutil.copyfile(path_to_data_file,path_to_temp_original)

#launch server and set him path to data file in /tmp dir
path_to_temp_copy = os.path.join(temp_dir,"datacopy.txt")
path_to_server_script = os.path.join(current_dir,"pyServer.py")
p = Popen(['python', path_to_server_script, path_to_temp_copy])
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
opened = False

while opened == False:
    try:
      s.connect(("localhost", 9999))
      opened = True
      s.close()
    except:
      pass

#launch LP and wait until it finishs his job
path_to_lp = os.path.join(current_dir,os.pardir,"lp","LP")
path_to_lp = os.path.realpath(path_to_lp)
os.system(path_to_lp + " -t 1 -s 1 -p 9999 -f " + path_to_temp_original)

#compare two files, to pass they should be identical
result = filecmp.cmp(path_to_temp_original,path_to_temp_copy)
if result == True:
    print("Files are equal")
else:
    print("Files are different")

#clear the resources
p.kill()
os.remove(path_to_temp_original)
os.remove(path_to_temp_copy)

exit( not result )



