#ifndef PROJECT_TRADER_H
#define PROJECT_TRADER_H

#include <map>
#include <string>
#include <mutex>
#include "banktraiding.h"
#include "recordqueue.h"

class trader
{
public:
    void UpdateInfo(const Bank& bank);
    void Arbitrage();
    void SendOrderToControlMarket(ArbitrageOrder& order);
    void RawMessagesListening();
    void Update();
    void WriteTradeLog();
    void NonStopArbitrage();

protected:
    std::map<std::string,Bank> info;
    std::mutex sync;
    RecordQueue<ArbitrageOrder> successOrders;
    RecordQueue<std::string> lines;

    void extractRecordsFromSocket(int sock_handle,uint8_t* buffer,uint32_t size);
};

#endif //PROJECT_TRADER_H
