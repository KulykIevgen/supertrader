//
// Created by kulyk on 15.09.17.
//

#include <stdexcept>
#include "csvparser.h"

Bank csvparser::FromStringToBank(const std::string& line)
{
    Bank result;

    unsigned long pos = 0;
    int number = 0;

    do
    {
        auto nextpos = line.find(',',pos);
        if (nextpos != std::string::npos)
        {
            std::string currentPart = line.substr(pos,nextpos - pos);
            switch (number)
            {
                case Parameters::LPID:
                    result.Name = currentPart;
                    break;
                case Parameters::BIDprice:
                    result.EurUsd.Bid.Price = std::stod(currentPart);
                    break;
                case Parameters::BIDTime:
                    result.EurUsd.Bid.Time = std::stoull(currentPart);
                    break;
                case Parameters::BIDDeliverycounter:
                    break;
                case Parameters::BIDvolume:
                    result.EurUsd.Bid.Volume = std::stoull(currentPart);
                    break;
                case Parameters::ASKprice:
                    result.EurUsd.Offer.Price = std::stod(currentPart);
                    break;
                case Parameters::ASKTime:
                    result.EurUsd.Offer.Time = std::stoull(currentPart);
                    break;
                case Parameters::ASKDeliverycounter:
                    break;
                case Parameters::ASKvolume:
                    result.EurUsd.Offer.Volume = std::stoull(currentPart);
                    break;
                default:
                    throw std::logic_error("Bad number of parameters: " + std::to_string(number));
            }
            number++;
            pos = nextpos + 1;
        }
        else if (number == Parameters::ASKvolume)
        {
            std::string currentPart = line.substr(pos);
            result.EurUsd.Offer.Volume = std::stoull(currentPart);
            number++;
            pos = std::string::npos;
        }
        else
        {
            pos = std::string::npos;
        }
    }
    while (pos < line.length());

    if (number <= Parameters::ASKvolume)
    {
        throw std::logic_error("Bad line format");
    }

    return result;
}
