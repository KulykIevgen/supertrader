#ifndef PROJECT_BANKTRAIDING_H
#define PROJECT_BANKTRAIDING_H

#include <string>
#include <limits>

typedef unsigned long long int uint128_t;

struct CurrencyInfo
{
    uint128_t Volume;
    double Price;
    uint128_t Time;
};

struct Currency
{
    CurrencyInfo Bid;
    CurrencyInfo Offer;
};

struct Bank
{
    std::string Name;
    Currency EurUsd;
    Bank():Name(""){ }
};

struct ArbitrageOrder
{
    std::string BidBankName;
    double BidPrice;
    std::string OfferBankName;
    double OfferPrice;
    uint128_t Volume;
    bool Accepted;
    ArbitrageOrder():BidBankName(""),
                     BidPrice(std::numeric_limits<double>::min()),
                     OfferBankName(""),
                     OfferPrice(std::numeric_limits<double>::max()),
                     Volume(0),
                     Accepted(false){ }
};

#endif //PROJECT_BANKTRAIDING_H
