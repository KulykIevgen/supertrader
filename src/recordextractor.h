#ifndef PROJECT_RECORDEXTRACTOR_H
#define PROJECT_RECORDEXTRACTOR_H

#include "recordqueue.h"

class recordextractor
{
public:
    static unsigned int ExtractRecords(RecordQueue<std::string>& queue,uint8_t* data,uint32_t size);
};


#endif //PROJECT_RECORDEXTRACTOR_H
