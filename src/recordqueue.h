#ifndef PROJECT_RECORDQUEUE_H
#define PROJECT_RECORDQUEUE_H

#include <queue>
#include <mutex>

template <typename T>
class RecordQueue
{
public:
    bool get(T& result)
    {
        std::lock_guard<decltype(lockObject)> guard(lockObject);
        if (internalContainer.empty())
        {
            return false;
        }

        result = std::move(internalContainer.front());
        internalContainer.pop();
        return true;
    }
    void insert(T element)
    {
        std::lock_guard<decltype(lockObject)> guard(lockObject);
        internalContainer.push(element);
    }

private:
    std::queue<T> internalContainer;
    std::mutex lockObject;
};

#endif //PROJECT_RECORDQUEUE_H
