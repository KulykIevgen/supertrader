#ifndef PROJECT_RECORDQUEUETEST_H
#define PROJECT_RECORDQUEUETEST_H

#include "gtest/gtest.h"

class recordqueuetest
{
public:
    static void extractFromEmptyQueue();
    static void putTwiceAndGetSecond();
};

TEST(recordqueuetest,extractFromEmptyQueue)
{
    recordqueuetest::extractFromEmptyQueue();
}

TEST(recordqueuetest,putTwiceAndGetSecond)
{
    recordqueuetest::putTwiceAndGetSecond();
}

#endif //PROJECT_RECORDQUEUETEST_H
