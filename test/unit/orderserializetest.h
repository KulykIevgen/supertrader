#ifndef PROJECT_ORDERSERIALIZETEST_H
#define PROJECT_ORDERSERIALIZETEST_H

#include <gtest/gtest.h>

class orderserializetest
{
public:
    static void serialize();
    static void deserialize();
    static void deserializeInvalidOrder();
};

TEST(orderserializetest,serialize)
{
    orderserializetest::serialize();
}

TEST(orderserializetest,deserialize)
{
    orderserializetest::deserialize();
}

TEST(orderserializetest,deserializeInvalidOrder)
{
    orderserializetest::deserializeInvalidOrder();
}

#endif //PROJECT_ORDERSERIALIZETEST_H
