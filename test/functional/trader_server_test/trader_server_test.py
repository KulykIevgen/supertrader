import os
from subprocess import Popen
import socket

#find test server application and client script
current_dir = os.path.dirname(os.path.realpath(__file__))
path_to_test_server = os.path.join(current_dir,"Traider_server_test")
client_path = os.path.join(current_dir,"simple_client.py")

#start server
print("Starting trading server")
server = Popen([path_to_test_server])
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
opened = False

#wait until server ready to listen
while opened == False:
    try:
        s.connect(("localhost", 9999))
        opened = True
        s.close()
    except:
        pass

print("Server started")
print("Starting clients one by one")
#start clients
client1 = Popen(['python', client_path])
client2 = Popen(['python', client_path])
client3 = Popen(['python', client_path])

#wait until clients finish their job
client1.wait()
client2.wait()
client3.wait()

print("Get clients responce: client1 = " + str(client1.returncode) + ", client2 = " + str(client2.returncode) +
      ", client3 = " + str(client3.returncode))
#get total result - Zero (0) if OK
result = client1.returncode + client2.returncode + client3.returncode

#kill server
server.kill()

#get the result to find out was the test successful
exit(result)
