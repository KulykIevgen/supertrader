#include <recordqueue.h>
#include "recordextractortest.h"
#include <recordextractor.h>

void recordextractortest::extractOneLine()
{
    RecordQueue<std::string> queue;
    uint8_t input[] = "my test string\r\n";
    auto lines = recordextractor::ExtractRecords(queue,input,sizeof(input) - 1);
    EXPECT_EQ(lines,static_cast<unsigned int> (1));
    std::string firstLine;
    queue.get(firstLine);
    EXPECT_EQ(firstLine,std::string("my test string"));
}

void recordextractortest::extractLineStartsWithSeparator()
{
    RecordQueue<std::string> queue;
    uint8_t input[] = "\r\nmy test string\r\n";
    auto lines = recordextractor::ExtractRecords(queue,input,sizeof(input) - 1);
    EXPECT_EQ(lines,static_cast<unsigned int> (1));
    std::string firstLine;
    queue.get(firstLine);
    EXPECT_EQ(firstLine,std::string("my test string"));
}

void recordextractortest::extractLinesWhereLastLineWithoutSeparator()
{
    RecordQueue<std::string> queue;
    uint8_t input[] = "\r\nmy test string\r\nhello";
    auto lines = recordextractor::ExtractRecords(queue,input,sizeof(input) - 1);
    EXPECT_EQ(lines,static_cast<unsigned int> (2));
    std::string firstLine;
    queue.get(firstLine);
    EXPECT_EQ(firstLine,std::string("my test string"));
    queue.get(firstLine);
    EXPECT_EQ(firstLine,std::string("hello"));
}
