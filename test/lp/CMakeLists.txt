PROJECT(LP)

set (SOURCE_FILES datapulsar.cpp socksender.cpp)
set (HEADER_FILES datapulsar.h)

add_executable(${PROJECT_NAME} ${HEADER_FILES} ${SOURCE_FILES})