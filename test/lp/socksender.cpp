#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <cstdlib>
#include <chrono>
#include <thread>

#include "datapulsar.h"

#define STRNGLEN 4096

int MakeConnection(char* ServerName,int port)
{
  int s;
  struct sockaddr_in ssin;
  struct hostent* hp;
  int PortNum = port;
  char strHlp[STRNGLEN] = {0};

  strcpy(strHlp,ServerName);
  if( (hp=gethostbyname(strHlp)) == NULL  )
  {
     return -1;
  }

  bzero(&ssin, sizeof(ssin));
  bcopy(hp->h_addr, &ssin.sin_addr, hp->h_length);
  ssin.sin_family = hp->h_addrtype;
  ssin.sin_port = htons(PortNum);

  if((s=socket(AF_INET, SOCK_STREAM, 0))==-1)
  {
     return -1;
  }

  if(connect(s,reinterpret_cast<const sockaddr*> ( &ssin ), sizeof(ssin))==-1)
  {
     return -1;
  }

  return s;
}

int main(int argc,char** argv)
{
    int opt = -1;
    uint32_t lineNumber = 1;
    int port = 80;
    unsigned long long int freequency = 1;
    std::string fileName;

    while ((opt = getopt(argc, argv, "s:t:p:f:")) != -1)
    {
        switch (opt) {
            case 's':
                lineNumber = static_cast<uint32_t> (std::stoi(std::string(optarg)));
                break;
            case 'p':
                port = std::stoi(optarg);
                break;
            case 't':
                freequency = std::stoull(optarg);
                break;
            case 'f':
                fileName = std::string(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s [-t milliseconds] [-s string-line numbers] [-p port number] [-f filename] \n",
                        argv[0]);
                std::exit(EXIT_FAILURE);
                break;
        }
    }

    std::cout << "Number of lines per request: " << lineNumber << std::endl;
    std::cout << "Port number for connection to server: " << port << std::endl;
    std::cout << "Freequency for request sending: " << freequency << " milliseconds" << std::endl;
    std::cout << "File with market data: " << fileName << std::endl;

    signal(SIGPIPE, SIG_IGN);
	DataPulsar pulsar(fileName);
	char localhost[] = "localhost";	
	while (true)
	{
        try
        {
            auto handle = MakeConnection(localhost, port);
            auto test = pulsar.ReadLines(lineNumber);
            send(handle, test.c_str(), test.length(), 0);
            close(handle);
            std::this_thread::sleep_for(std::chrono::milliseconds(freequency));
        }
        catch(const std::logic_error&)
        {
            break;
        }
	}
	return 0;
}
