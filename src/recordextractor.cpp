#include <cstring>
#include <iostream>
#include "recordextractor.h"

unsigned int recordextractor::ExtractRecords(RecordQueue<std::string>& queue, uint8_t* data, uint32_t size)
{
    const uint8_t revert = '\r';
    const uint8_t newline = '\n';
    std::string line;
    unsigned int linesFound = 0;

    auto IsSymbolReadable = [size](uint32_t offset) -> bool
    {
        return offset < size;
    };

    auto IsSymbolMatch = [data,size,IsSymbolReadable](uint32_t offset,uint8_t symbol) -> bool
    {
        if (!IsSymbolReadable(offset))
        {
            return false;
        }
        return data[offset] == symbol;
    };

    auto ExtractString = [&line,&queue,&linesFound,data](uint32_t Begin,uint32_t Size) -> void
    {
        if (Size != 0)
        {
            line.resize(Size);
            memcpy(const_cast<char *>( line.data()), &data[Begin], Size);
            queue.insert(std::move(line));
            linesFound++;
        }
    };

    uint32_t leftcorner = 0, rightcorner = 0;
    for (uint32_t i = 0;i < size;)
    {
        if (IsSymbolMatch(i,revert) && IsSymbolMatch(i + 1,newline))
        {
            auto lineSize = rightcorner - leftcorner;
            ExtractString(leftcorner,lineSize);
            i += 2;
            leftcorner = rightcorner = i;
        }
        else
        {
            i++;
            rightcorner++;
        }
    }

    if (leftcorner != rightcorner)
    {
        ExtractString(leftcorner,rightcorner - leftcorner);
    }

    return linesFound;
}
