#include "trader.h"
#include "recordextractor.h"
#include "csvparser.h"
#include "orderserialize.h"

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <future>
#include <csignal>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <fstream>
#include <iostream>
#include <iomanip>

void trader::UpdateInfo(const Bank& bank)
{
    std::lock_guard<decltype(sync)> lock(sync);

    try
    {
        Bank& TargetBankInfo = info.at(bank.Name);
        if (TargetBankInfo.EurUsd.Bid.Time < bank.EurUsd.Bid.Time)
        {
            TargetBankInfo.EurUsd.Bid = bank.EurUsd.Bid;
        }

        if (TargetBankInfo.EurUsd.Offer.Time < bank.EurUsd.Offer.Time)
        {
            TargetBankInfo.EurUsd.Offer = bank.EurUsd.Offer;
        }
    }
    catch (const std::out_of_range&)
    {
        info[bank.Name] = bank;
    }
}

void trader::Arbitrage()
{
    std::lock_guard<decltype(sync)> lock(sync);
    ArbitrageOrder arbitrageOrder;

    for (const auto& bank:info)
    {
        if (bank.second.EurUsd.Bid.Price > arbitrageOrder.BidPrice)
        {
            arbitrageOrder.BidPrice = bank.second.EurUsd.Bid.Price;
            arbitrageOrder.BidBankName = bank.second.Name;
        }

        if (bank.second.EurUsd.Offer.Price < arbitrageOrder.OfferPrice)
        {
            arbitrageOrder.OfferPrice = bank.second.EurUsd.Offer.Price;
            arbitrageOrder.OfferBankName = bank.second.Name;
        }
    }

    if (arbitrageOrder.BidPrice == 0 || arbitrageOrder.OfferPrice == 0)
        return;

    if (arbitrageOrder.BidPrice > arbitrageOrder.OfferPrice)
    {
        arbitrageOrder.Volume = std::min(info[arbitrageOrder.OfferBankName].EurUsd.Offer.Volume,
                                         info[arbitrageOrder.BidBankName].EurUsd.Bid.Volume);
        if (arbitrageOrder.Volume > 0)
        {
            SendOrderToControlMarket(arbitrageOrder);
            if (arbitrageOrder.Accepted)
            {
                info[arbitrageOrder.OfferBankName].EurUsd.Offer.Volume -= arbitrageOrder.Volume;
                info[arbitrageOrder.BidBankName].EurUsd.Bid.Volume -= arbitrageOrder.Volume;
                successOrders.insert(arbitrageOrder);
            }
        }
    }
}

void trader::SendOrderToControlMarket(ArbitrageOrder& order)
{
    order.Accepted = true;
}

void trader::RawMessagesListening()
{
    const uint16_t port = 9999;
    const uint32_t default_message_size = 2000;

    int socket_desc , client_sock , c;
    struct sockaddr_in server , client;
    uint8_t client_message[default_message_size] = {0};

    signal(SIGPIPE, SIG_IGN);
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        throw std::logic_error("Could not create socket");
    }
    int i = 1;
    setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, (void *)&i, sizeof(i));
    setsockopt(socket_desc, IPPROTO_TCP, TCP_QUICKACK, (void *)&i, sizeof(i));

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( port );

    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        throw std::logic_error("bind failed. Error");
    }

    listen(socket_desc , 100);
    c = sizeof(struct sockaddr_in);

    while (true)
    {
        client_sock = accept(socket_desc, (struct sockaddr *) &client, (socklen_t *) &c);
        if (client_sock >= 0)
        {
            extractRecordsFromSocket(client_sock,client_message,default_message_size);
            close(client_sock);
        }
        else
        {
            throw std::logic_error("Accept connection failed: " + std::string(strerror(errno)));
        }
    }
}

void trader::extractRecordsFromSocket(int sock_handle, uint8_t* buffer, uint32_t size)
{
    ssize_t read_size = 0;
    while ((read_size = recv(sock_handle, buffer, size, 0)) > 0)
    {
        if (read_size > 0)
        {
            recordextractor::ExtractRecords(lines, buffer, static_cast<uint32_t> (read_size));
        }
        else
        {
            break;
        }
    }
}

void trader::Update()
{
    while (true)
    {
        std::string line;
        if (lines.get(line))
        {
            const Bank& bank = csvparser::FromStringToBank(line);
            UpdateInfo(bank);
        }
    }
}

void trader::WriteTradeLog()
{
    std::ofstream log("/tmp/logtrade.txt");
    log << std::setw(5) << "|Accept "
        << std::setw(14) << "|Bid bank "
        << std::setw(10) << "|Bid Price "
        << std::setw(14) << "|Offer bank "
        << std::setw(10) << "|Offer Price "
        << std::setw(10) << "|Volume "
        << std::setw(5) << "|Total earned"
        << std::endl;
    double earned = 0;
    while (true)
    {
        ArbitrageOrder order;
        if (successOrders.get(order))
        {
            const std::string &serializedData = orderserialize::Serialize(order);
            earned += order.Volume * (order.BidPrice - order.OfferPrice);
            log << serializedData << std::setw(5) << earned << std::endl;
            log.flush();
        }
    }
}

void trader::NonStopArbitrage()
{
    while (true)
    {
        Arbitrage();
    }
}
