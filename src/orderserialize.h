#ifndef PROJECT_ORDERSERIALIZE_H
#define PROJECT_ORDERSERIALIZE_H

#include <string>
#include "banktraiding.h"

class orderserialize
{
public:
    static std::string Serialize(const ArbitrageOrder& order);
    static ArbitrageOrder Deserialize(const std::string& serialized);
};


#endif //PROJECT_ORDERSERIALIZE_H
