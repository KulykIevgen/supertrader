#include <csvparser.h>
#include "csvparsertest.h"

void csvparser_test::stringParsingToBank()
{
    std::string line = "FXSPOTSTREAM,1.35574,1359975690335010,1,500000,1.3558,1359975690335010,1,500000";
    Bank bank = csvparser::FromStringToBank(line);
    EXPECT_EQ(bank.Name,"FXSPOTSTREAM");
    EXPECT_EQ(bank.EurUsd.Bid.Price,1.35574);
    EXPECT_EQ(bank.EurUsd.Bid.Time,static_cast<unsigned long long> (1359975690335010));
}

void csvparser_test::wrongStringFormatOfBank()
{
    std::string line = "I'm a bad formatted string :)";
    EXPECT_THROW(csvparser::FromStringToBank(line),std::logic_error);
}

void csvparser_test::alotOfCommas()
{
    std::string line = "I'm a bad formatted string,,,,,,,,,,,,,,,,,,,,,,,,,,,,,, :)";
    EXPECT_THROW(csvparser::FromStringToBank(line),std::logic_error);
}
