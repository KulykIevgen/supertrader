#include "traderservertest.h"
#include <trader.h>

void traderservertest::LaunchServerAndListen()
{
    trader SuperTrader;
    SuperTrader.RawMessagesListening();
}

int main(int argc,char** argv)
{
    traderservertest::LaunchServerAndListen();
    return 0;
}