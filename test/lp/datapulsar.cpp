#include <fstream>
#include <stdexcept>
#include "datapulsar.h"

DataPulsar::DataPulsar(const std::string& data)
{
	currentLine = 0;
	std::ifstream fileWithData(data);
	while (!fileWithData.eof())
	{
		std::string tmp;
		getline(fileWithData,tmp);
		lines.push_back(std::move(tmp));		
	}
}

const std::string& DataPulsar::ReadLine() const
{
	if (currentLine < lines.size())
		return lines[currentLine++];
	else
		throw std::logic_error("no more data");
}

std::string DataPulsar::ReadLines(uint32_t numberOfLines) const
{
	std::string result;
	for (uint32_t i = 0;i < numberOfLines;i++)
	{
		result += ReadLine();
	}
	return result;
}
