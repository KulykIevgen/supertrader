#ifndef PROJECT_CSVPARSERTEST_H
#define PROJECT_CSVPARSERTEST_H

#include "gtest/gtest.h"
#include <string>

class csvparser_test
{
public:
    static void stringParsingToBank();
    static void wrongStringFormatOfBank();
    static void alotOfCommas();
};

TEST(csvparser_test,stringParsingToBank)
{
    csvparser_test::stringParsingToBank();
}

TEST(csvparser_test,wrongStringFormatOfBank)
{
    csvparser_test::wrongStringFormatOfBank();
}

TEST(csvparser_test,alotOfCommas)
{
    csvparser_test::alotOfCommas();
}

#endif //PROJECT_CSVPARSERTEST_H
