import SocketServer
import sys
import os

handle_file = 0

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The request handler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(1024).strip()
        #print "{} wrote:".format(self.client_address[0])
        if len(self.data) > 2:
            print self.data
            handle_file.write(self.data + "\r\n")
            handle_file.flush()
            os.fsync(handle_file)

if __name__ == "__main__":
    HOST, PORT = "localhost", 9999
    data_file_to_set_input_data = sys.argv[1]
    handle_file = open(data_file_to_set_input_data,"w")

    # Create the server, binding to localhost on port 9999
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
