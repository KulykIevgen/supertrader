#include <thread>
#include "trader.h"

int main(int argc,char** argv)
{
    trader SuperTrader;
    auto message_listener = std::thread(&trader::RawMessagesListening,std::ref(SuperTrader));
    message_listener.detach();

    auto info_updater = std::thread(&trader::Update,std::ref(SuperTrader));
    info_updater.detach();

    auto arbitr = std::thread(&trader::NonStopArbitrage,std::ref(SuperTrader));
    arbitr.detach();

    auto logger = std::thread(&trader::WriteTradeLog,std::ref(SuperTrader));
    logger.join();

    return 0;
}
