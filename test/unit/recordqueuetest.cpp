#include "recordqueuetest.h"
#include "recordqueue.h"
#include <string>

void recordqueuetest::extractFromEmptyQueue()
{
    RecordQueue<int> queue;
    int output = 0;
    EXPECT_FALSE(queue.get(output));
}

void recordqueuetest::putTwiceAndGetSecond()
{
    RecordQueue<std::string> queue;
    queue.insert(std::string("hello"));
    queue.insert(std::string("world"));

    std::string result;
    EXPECT_TRUE(queue.get(result));
    EXPECT_TRUE(queue.get(result));
    EXPECT_FALSE(queue.get(result));
    EXPECT_EQ(std::string("world"),result);
}
