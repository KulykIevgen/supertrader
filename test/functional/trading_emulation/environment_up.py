import os
from subprocess import Popen
import socket

#find test data for LP
current_dir = os.path.dirname(os.path.realpath(__file__))
one = os.path.join(current_dir,"one.txt")
two = os.path.join(current_dir,"two.txt")
three = os.path.join(current_dir,"three.txt")

#find LP client
path_to_lp = os.path.join(current_dir,os.pardir,os.pardir,"lp","LP")
path_to_lp = os.path.realpath(path_to_lp)

#find supertrader server
path_to_supertrader = os.path.join(current_dir,os.pardir,os.pardir,os.pardir,"src","supertrader")
path_to_supertrader = os.path.realpath(path_to_supertrader)

#launch supertrader
supertrader = Popen([path_to_supertrader])

#wait until supertrader ready to listen
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
opened = False
while opened == False:
    try:
        s.connect(("localhost", 9999))
        opened = True
        s.close()
    except:
        pass

#launch 3 lp clients
lp_one = Popen([path_to_lp,'-t','1','-s','1','-p','9999','-f',one])
lp_two = Popen([path_to_lp,'-t','1','-s','1','-p','9999','-f',two])
lp_three = Popen([path_to_lp,'-t','1','-s','1','-p','9999','-f',three])

#wait while clients send all data
lp_one.wait()
lp_two.wait()
lp_three.wait()

#clear resources - kill supertrader server
supertrader.kill()
exit(0)