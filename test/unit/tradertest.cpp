#include "tradertest.h"

std::map<std::string, Bank> tradertest::get()
{
    return this->info;
}

void tradertest::addBankToClearDatabase()
{
    Bank bank;
    bank.Name = std::string("TEST");
    bank.EurUsd.Bid.Time = 1000;
    bank.EurUsd.Bid.Price = 100;
    bank.EurUsd.Bid.Volume = 10;
    bank.EurUsd.Offer.Time = 2000;
    bank.EurUsd.Offer.Price = 200;
    bank.EurUsd.Offer.Volume = 20;

    tradertest TestingObject;
    TestingObject.UpdateInfo(bank);
    auto information = TestingObject.get();
    EXPECT_EQ(information[bank.Name].EurUsd.Bid.Time,static_cast<uint128_t> (1000));
    EXPECT_EQ(information[bank.Name].EurUsd.Offer.Volume,static_cast<uint128_t> (20));
}

void tradertest::modifyDataBaseWithNewerBid()
{
    Bank bank;
    bank.Name = std::string("TEST");
    bank.EurUsd.Bid.Time = 1000;
    bank.EurUsd.Bid.Price = 100;
    bank.EurUsd.Bid.Volume = 10;
    bank.EurUsd.Offer.Time = 2000;
    bank.EurUsd.Offer.Price = 200;
    bank.EurUsd.Offer.Volume = 20;

    tradertest TestingObject;
    TestingObject.UpdateInfo(bank);

    bank.EurUsd.Bid.Time = 3000;
    bank.EurUsd.Bid.Price = 300;
    bank.EurUsd.Bid.Volume = 10;
    TestingObject.UpdateInfo(bank);

    auto information = TestingObject.get();
    EXPECT_EQ(information[bank.Name].EurUsd.Bid.Time,static_cast<uint128_t> (3000));
    EXPECT_EQ(information[bank.Name].EurUsd.Bid.Price,static_cast<double> (300));
}

void tradertest::makeSimpleArbitrage()
{
    Bank usa;
    usa.Name = std::string("USA");
    usa.EurUsd.Bid.Time = 1000;
    usa.EurUsd.Bid.Price = 100;
    usa.EurUsd.Bid.Volume = 10;
    usa.EurUsd.Offer.Time = 2000;
    usa.EurUsd.Offer.Price = 200;
    usa.EurUsd.Offer.Volume = 20;

    Bank ukr = usa;
    ukr.Name = std::string("UKR");
    ukr.EurUsd.Bid.Price = 500;
    ukr.EurUsd.Offer.Price = 501;

    tradertest TestingObject;
    TestingObject.UpdateInfo(usa);
    TestingObject.UpdateInfo(ukr);
    TestingObject.Arbitrage();

    auto information = TestingObject.get();
    EXPECT_EQ(information[usa.Name].EurUsd.Offer.Volume,static_cast<uint128_t> (10));
    EXPECT_EQ(information[ukr.Name].EurUsd.Bid.Volume,static_cast<uint128_t> (0));
}
