#include "orderserializetest.h"
#include <orderserialize.h>

void orderserializetest::serialize()
{
    ArbitrageOrder order;
    order.BidBankName = order.OfferBankName = std::string("USA");
    order.OfferPrice = order.BidPrice = 10;
    order.Volume = 1;
    order.Accepted = true;
    auto&& result = orderserialize::Serialize(order);
    EXPECT_EQ(result,std::string("    1           USA          10           USA          10         1"));
}

void orderserializetest::deserialize()
{
    std::string serialized("0 USA 10 UKR 1.10 1");
    ArbitrageOrder&& order = orderserialize::Deserialize(serialized);
    EXPECT_FALSE(order.Accepted);
    EXPECT_EQ(order.OfferBankName,std::string("UKR"));
}

void orderserializetest::deserializeInvalidOrder()
{
    std::string serialized("0 USA 10");
    ArbitrageOrder&& order = orderserialize::Deserialize(serialized);
    EXPECT_FALSE(order.Accepted);
    EXPECT_EQ(order.OfferBankName,std::string(""));
    EXPECT_EQ(order.Volume,static_cast<uint128_t> (0));
}
