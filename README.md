# SuperTrader

Sample trading tool that executes inside test environment and simulates arbitrage trading

## Pre Requirements
- sudo apt-get update
- sudo apt install cmake
- sudo apt install git

(On Ubuntu 16.04 and later python2 is already installed)

## How To Build
- git clone https://KulykIevgen@bitbucket.org/KulykIevgen/supertrader.git
- cd build
- cmake ..
- make -j

## How To Test
- ctest -VV (After build stay in a build directory)

## Breaf description

![Scheme](images/trading.png)

- SuperTrader is our target application. 
- LP (Liquidity provider) has to send data about bids and offers.
- Market Contol should allow (or not) trading orders.
- Log is just a txt file in /tmp/ dir.

## More info

There is small cheet - no Market Contol component. Everything is allowed :)
