#ifndef PROJECT_CSVPARSER_H
#define PROJECT_CSVPARSER_H

#include <string>
#include "banktraiding.h"

enum Parameters
{
    LPID,
    BIDprice,
    BIDTime,
    BIDDeliverycounter,
    BIDvolume,
    ASKprice,
    ASKTime,
    ASKDeliverycounter,
    ASKvolume
};

class csvparser
{
public:
    static Bank FromStringToBank(const std::string& line);
};

#endif //PROJECT_CSVPARSER_H
