#include <sstream>
#include "orderserialize.h"
#include <iomanip>

std::string orderserialize::Serialize(const ArbitrageOrder& order)
{
    std::ostringstream stream;
    stream << std::setw(5) << order.Accepted
           << std::setw(14) << order.BidBankName
           << std::setw(12) << order.BidPrice
           << std::setw(14) << order.OfferBankName
           << std::setw(12) << order.OfferPrice
           << std::setw(10) << order.Volume;
    return stream.str();
}

ArbitrageOrder orderserialize::Deserialize(const std::string& serialized)
{
    std::istringstream stream(serialized);
    ArbitrageOrder order;

    stream >> order.Accepted
           >> order.BidBankName
           >> order.BidPrice
           >> order.OfferBankName
           >> order.OfferPrice
           >> order.Volume;

    return order;
}
