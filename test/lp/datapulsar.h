#ifndef DATA_PULSAR
#define DATA_PULSAR

#include <vector>
#include <string>
#include <stdint.h>

class DataPulsar
{
public:
	DataPulsar(const std::string& data);
	const std::string& ReadLine() const;
	std::string ReadLines(uint32_t numberOfLines) const;
private:
	std::vector<std::string> lines;
	mutable uint32_t currentLine;
};

#endif
