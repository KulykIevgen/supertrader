#ifndef PROJECT_RECORDEXTRACTORTEST_H
#define PROJECT_RECORDEXTRACTORTEST_H

#include "gtest/gtest.h"

class recordextractortest
{
public:
    static void extractOneLine();
    static void extractLineStartsWithSeparator();
    static void extractLinesWhereLastLineWithoutSeparator();
};

TEST(recordextractortest,extractOneLine)
{
    recordextractortest::extractOneLine();
}

TEST(recordextractortest,extractLineStartsWithSeparator)
{
    recordextractortest::extractLineStartsWithSeparator();
}

TEST(recordextractortest,extractLinesWhereLastLineWithoutSeparator)
{
    recordextractortest::extractLinesWhereLastLineWithoutSeparator();
}

#endif //PROJECT_RECORDEXTRACTORTEST_H
