#ifndef PROJECT_TRADERTEST_H
#define PROJECT_TRADERTEST_H

#include "gtest/gtest.h"
#include <trader.h>

class tradertest:public trader
{
public:
    static void addBankToClearDatabase();
    static void modifyDataBaseWithNewerBid();
    static void makeSimpleArbitrage();

    std::map<std::string,Bank> get();
};

TEST(tradertest,addBankToClearDatabase)
{
    tradertest::addBankToClearDatabase();
}

TEST(tradertest,modifyDataBaseWithNewerBid)
{
    tradertest::modifyDataBaseWithNewerBid();
}

TEST(tradertest,makeSimpleArbitrage)
{
    tradertest::makeSimpleArbitrage();
}

#endif //PROJECT_TRADERTEST_H
